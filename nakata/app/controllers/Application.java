package controllers;

import play.*;
import play.mvc.*;

import views.html.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import play.libs.Json;
import java.util.*;


public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }


    public static Result getVariables() {


        ArrayList<Hashtable<String, String>> listagamers= new ArrayList<Hashtable<String, String>>();


          for (int i=0; i < 3; i++ ) {

          	  Hashtable<String, String> obj = new Hashtable<String, String>();


          	  Random rnd = new Random();
          	  


          	  obj.put( "imagen" , ("imagen" + String.valueOf(i)) );
	          obj.put("nombre", ("gamer" + String.valueOf(i)));
	          obj.put("variable", String.valueOf( (int) (rnd.nextDouble() * 100 + 1)) );


	          obj.put( "imagen2" , ("imagen" + String.valueOf(i)) );
	          obj.put("nombre2", ("gamer" + String.valueOf(i)));
	          obj.put("variable2", String.valueOf( (int) (rnd.nextDouble() * 100 + 1)) );



	          if(i%2 == 0){
	          	 obj.put("equipo","1");

	          }else{
	          	 obj.put("equipo","2");
	          }

	          listagamers.add(obj);
   	
          }


        
        ObjectNode json = Json.newObject();
        json.put("gamers", Json.toJson(listagamers));
        return ok(json);
    }



}
