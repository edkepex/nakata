package models;

import javax.persistence.*;
import play.data.format.Formats;
import play.db.ebean.Model;
import java.util.*;
import com.avaje.ebean.Page;

@Entity
public class Tbldatagame extends Model {

  @Id
  private Long id;


  private String idRow;
  private int window;


  public Long getId(){
    return id;
  }

  public void setId(Long id){
    this.id = id;
  }

  public String getIdRow(){
    return idRow;
  }

  public void setIdRow(String idRow){
    this.idRow = idRow;
  }

  public int getWindow(){
    return window;
  }

  public void setWindow(int window){
    this.window = window;
  }


  
  //public static Model.Finder<Long,Estado> find = new Model.Finder<Long, Estado>(Long.class, Estado.class);
  
}